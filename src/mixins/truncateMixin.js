export default {
  filters: {
    truncate(value, limit) {
      if (value.length <= limit) {
        return value;
      }
      return `${value.substring(0, (limit - 3))}...`;
    },
  },
};
