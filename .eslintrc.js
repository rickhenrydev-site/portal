module.exports = {
  root: true,

  env: {
    node: true,
  },

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/attribute-hyphenation': 'warning',
    'vue/html-closing-bracket-newline': 'warning',
    'vue/html-closing-bracket-spacing': 'error',
    'vue/html-end-tags': 'error',
    'vue/html-indent': 'error',
    'vue/html-quotes': 'error',
    'vue/max-attributes-per-line': 'warning',
    'vue/no-template-shadow': 'warning',
    'vue/require-prop-types': 'warning',
    'vue/eqeqeq': 'error',
    'vue/match-component-file-name': 'error',
  },

  parserOptions: {
    parser: 'babel-eslint',
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
};
